import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner Num1 = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]:");

        int MyNum = Num1.nextInt();
        if (MyNum == 1) {
            Scanner P1 = new Scanner(System.in);
            System.out.print("Please input number:");
            int Number1 = P1.nextInt();
            for (int i = 0; i < Number1 + 1; i++) {
                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if (MyNum == 2) {
            Scanner P2 = new Scanner(System.in);
            System.out.print("Please input number:");
            int Number2 = P2.nextInt();
            for (int i = Number2; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else if (MyNum > 5) {
            System.out.println("Error: Please input number between 1-5");
        } else {
            System.out.println("Bye bye!!!");
        }
    }
}
