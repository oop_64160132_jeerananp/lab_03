import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner Num1 = new Scanner(System.in);
        int Number1;

        System.out.print("Please input first number:");
        Number1 = Num1.nextInt();

        Scanner Num2 = new Scanner(System.in);
        int Number2;

        System.out.print("Please input second number:");
        Number2 = Num2.nextInt();

        if (Number1 <= Number2) {
            int i = Number1;
            while (i <= Number2) {
                System.out.print(" ");
                System.out.print(i);
                i = i + 1;
            }
        } else if (Number1 == Number2) {
            System.out.println(Number1);
        } else {
            System.out.println("Error");
        }
    }
}
