import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner MyNum = new Scanner(System.in);
        int Number;
        int Sum = 0;
        int i = 0;
        float Avg = 0;
        do {
            System.out.print("Please input number:");
            Number = MyNum.nextInt();

            Sum = Sum + Number;
            i = i + 1;
            Avg = Sum / i;
            if (Number != 0) {
                System.out.println("Sum:" + Sum + ", " + "Avg:" + Avg);
            } else {
                break;
            }
        } while (Number != 0);
        System.out.println("Bye");
    }
}
