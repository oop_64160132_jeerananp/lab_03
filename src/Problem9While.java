import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner MyNum = new Scanner(System.in);
        System.out.print("Please input n: ");

        int Num = MyNum.nextInt();
        int i = 1;
        while (i < Num + 1) {
            int j = 1;
            while (j < Num + 1) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}
